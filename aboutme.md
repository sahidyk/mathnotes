---
layout: page
title: About me
subtitle: Why you'd want to go on a date with me
use_math: true
---

My name is Inigo Montoya. I have the following qualities:

- I rock a great mustache
- I'm extremely loyal to my family

What else do you need?

### my history

To be honest, I'm having some trouble remembering right now, so why don't you just watch [my movie](https://en.wikipedia.org/wiki/The_Princess_Bride_%28film%29) and it will answer **all** your questions.

### Test $`\LaTeX`$
Teks matematika dengan $`\LaTeX`$ dapat ditampilkan dengan mudah di Web Github yang diakses menggunakan Google Chrome atau Microsoft Edge, dengan cara menginstal Ekstensi Chrome [**XHub**](https://github.com/nschloe/xhub). Ekstensi ini juga dapat diinstal ke Microsoft Edge dari situs Ekstensi Google Chrome. Ekstensi XHub menggunakan KaTeX untuk memproses/menampilkan matematika dengan LaTeX. 

Teks matematika di file Markdown yang disimpan di Github ditulis dengan format ```$`\int_0^{\infty} \sum_{k=1}^n \binom{n}{k} x^k \ dx `$``` untuk *inline* dan

````
```math
\int_0^{\infty} \sum_{k=1}^n \binom{n}{k} x^k \ dx 
```
````
untuk *display*.

Teks matematika di tengah baris: $`\int_0^{\infty} \sum_{k=1}^n \binom{n}{k} x^k \ dx `$.

Teks matematika pada baris terpisah:
```math
\int_0^{\infty} \sum_{k=1}^n \binom{n}{k} x^k \ dx
```
Kutipan dari pemrogram Xhub:

Use inline and display math like

````markdown
Display math:

```math
e^{i\pi} + 1 = 0
```

and line math $`a^2 + b^2 = c^2`$.
````

e.g., _Cauchy's Theorem_:

Let $`U`$ be an open subset of the complex plane $`\mathbb{C}`$, and suppose the closed
disk $`D`$ defined as

```math
D = \bigl\{z:|z-z_{0}|\leq r\bigr\}
```

is completely contained in $`U`$. Let $`f: U\to\mathbb{C}`$ be a holomorphic function,
and let $`\gamma`$ be the circle, oriented counterclockwise, forming the boundary of
$`D`$. Then for every $`a`$ in the interior of $`D`$,

```math
f(a) = \frac{1}{2\pi i} \oint _{\gamma}\frac{f(z)}{z-a} dz.
```

#### Chart.js

Use `chartjs` language blocks with the chart config as JSON and optionally canvas
attributes,

````markdown
```chartjs
{
  "config": { ... },
  "canvas": { ... }
}
```
````

e.g.,

```chartjs
{
  "config": {
    "type": "line",
    "data": {
      "labels": [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
      "datasets": [{
          "data": [86,114,106,106,107,111,133,221,783,2478],
          "label": "Africa",
          "borderColor": "#3e95cd",
          "fill": false
        }, {
          "data": [282,350,411,502,635,809,947,1402,3700,5267],
          "label": "Asia",
          "borderColor": "#8e5ea2",
          "fill": false
        }, {
          "data": [168,170,178,190,203,276,408,547,675,734],
          "label": "Europe",
          "borderColor": "#3cba9f",
          "fill": false
        }, {
          "data": [40,20,10,16,24,38,74,167,508,784],
          "label": "Latin America",
          "borderColor": "#e8c3b9",
          "fill": false
        }, {
          "data": [6,3,2,2,7,26,82,172,312,433],
          "label": "North America",
          "borderColor": "#c45850",
          "fill": false
        }
      ]
    },
    "options": {
      "title": {
        "display": true,
        "text": "World population per region (in millions)"
      }
    }
  }
}
```

#### Plotly

Use `plotly` language blocks with the plot config as JSON

````markdown
```plotly
{
  "data": [ ... ],
  "layout": { ... }
}
```
````

e.g.,

```plotly
{
  "data": [
    {
      "line": {"shape": "linear"},
      "mode": "lines+markers",
      "name": "linear",
      "type": "scatter",
      "x": [1, 2, 3, 4, 5],
      "y": [1, 3, 2, 3, 1],
      "hoverinfo": "name"
    },
    {
      "line": {"shape": "spline"},
      "mode": "lines+markers",
      "name": "spline",
      "type": "scatter",
      "x": [1, 2, 3, 4, 5],
      "y": [6, 8, 7, 8, 6],
      "text": ["tweak line smoothness<br>with 'smoothing' in line object"],
      "hoverinfo": "text+name"
    },
    {
      "line": {"shape": "vhv"},
      "mode": "lines+markers",
      "name": "vhv",
      "type": "scatter",
      "x": [1, 2, 3, 4, 5],
      "y": [11, 13, 12, 13, 11],
      "hoverinfo": "name"
    },
    {
      "line": {"shape": "hvh"},
      "mode": "lines+markers",
      "name": "hvh",
      "type": "scatter",
      "x": [1, 2, 3, 4, 5],
      "y": [16, 18, 17, 18, 16],
      "hoverinfo": "name"
    },
    {
      "line": {"shape": "vh"},
      "mode": "lines+markers",
      "name": "vh",
      "type": "scatter",
      "x": [1, 2, 3, 4, 5],
      "y": [21, 23, 22, 23, 21],
      "hoverinfo": "name"
    },
    {
      "line": {"shape": "hv"},
      "mode": "lines+markers",
      "name": "hv",
      "type": "scatter",
      "x": [1, 2, 3, 4, 5],
      "y": [26, 28, 27, 28, 26],
      "hoverinfo": "name"
    }
  ],
  "layout": {
    "legend": {
      "y": 0.5,
      "font": {"size": 16},
      "traceorder": "reversed"
    }
  }
}
```

#### Mermaid

Use `mermaid` language blocks with the config, e.g.,

````markdown
```mermaid
sequenceDiagram
    participant Alice
    participant Bob
    Alice->>John: Hello John, how are you?
    loop Healthcheck
        John->>John: Fight against hypochondria
    end
    Note right of John: Rational thoughts <br/>prevail!
    John-->>Alice: Great!
    John->>Bob: How about you?
    Bob-->>John: Jolly good!
```
````

```mermaid
sequenceDiagram
    participant Alice
    participant Bob
    Alice->>John: Hello John, how are you?
    loop Healthcheck
        John->>John: Fight against hypochondria
    end
    Note right of John: Rational thoughts <br/>prevail!
    John-->>Alice: Great!
    John->>Bob: How about you?
    Bob-->>John: Jolly good!
```

#### YouTube embed

Here's an embedded YouTube video:

````markdown
```youtube-embed
{
   "width": "560",
   "height": "315",
   "src": "https://www.youtube.com/embed/dQw4w9WgXcQ",
   "title": "YouTube video player",
   "frameborder": "0",
   "allow": "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture",
   "allowfullscreen": true
}
```
````

```youtube-embed
{
   "width": "560",
   "height": "315",
   "src": "https://www.youtube.com/embed/dQw4w9WgXcQ",
   "title": "YouTube video player",
   "frameborder": "0",
   "allow": "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture",
   "allowfullscreen": true
}
```

### Badge

[![xhub](https://img.shields.io/badge/Rendered%20with-xhub-f2eecb?style=flat-square)](https://chrome.google.com/webstore/detail/xhub/anidddebgkllnnnnjfkmjcaallemhjee)

```markdown
[![xhub](https://img.shields.io/badge/Rendered%20with-xhub-f2eecb?style=flat-square)](https://chrome.google.com/webstore/detail/xhub/anidddebgkllnnnnjfkmjcaallemhjee)
```
